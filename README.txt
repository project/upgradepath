DESCRIPTION
===========

This project implements a drush command that runs the a set of common steps
to get a database up to date with the exported configuration in code.

There is no .module nor .info files because this is not a module. It is a Drush
command. Drush can find commands in certain directories such as $HOME/.drush/
or sites/all/drush. Run `drush topic docs-commands` on a terminal to see other
places where this project can be placed so Drush can discover it. Depending on
your needs you may decide to leave this command with or without your project's
versioned code.

REQUIREMENTS
============

* Drush 6 or higher.
* Registry Rebuild.
* Features.

INSTALLATION
============

First, make sure that you meet the requiremens by reading the previous section.

Then, go to your project's root directory and run the following command:

$ drush dl upgradepath

This will normally download the command into `/path-to-drush/drush/commands/upgradepath`.
If you want to move it somewhere within your project so it is under version control,
move it to sites/all/drush or run drush dl --destination=sites/all/drush upgradepath..

USAGE
=====

$ cd /path/to/drupal/root
$ drush cc drush # This is needed for the first time since we added a new command.
$ drush upgradepath

CUSTOMIZING
===========

Drush's API is very flexible. You can hook into before and after the command, plus
take action when errors occur. Have a look at drush topic docs-api for further
details on which hooks you can implement from your commands to alter the
upgradepath.
